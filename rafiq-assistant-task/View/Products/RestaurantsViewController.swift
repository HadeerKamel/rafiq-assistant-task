//
//  ProductsViewController.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/1/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import UIKit

class RestaurantsViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var productsTableView: UITableView!
    //MARK: - Properties -
    var restaurants : [RestaurantModel]  = []
    //MARK: - Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        productsTableView.delegate = self
        productsTableView.dataSource = self
        productsTableView.reloadData()
        productsTableView.register(UINib.init(nibName: "RestaurantTableViewCell", bundle: nil), forCellReuseIdentifier: "RestaurantTableViewCell")
    }
    

    //MARK: - Actions -

    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: - tableViewDelegate -
extension RestaurantsViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantTableViewCell", for: indexPath) as! RestaurantTableViewCell
        cell.init_(data: restaurants[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
}
