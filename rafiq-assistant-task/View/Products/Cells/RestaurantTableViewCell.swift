//
//  RestaurantTableViewCell.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/14/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import UIKit
import Kingfisher
class RestaurantTableViewCell: UITableViewCell {

    //MARK: - Outlets -
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    //MARK: - Properties
    
    var data : RestaurantModel!{
        didSet{
            setupView()
        }
    }
    
    func init_(data: RestaurantModel){
        self.data = data
    }
    private func setupView(){
        guard let data_ = data else {
            return
        }
        if let url = URL(string: data_.imageURL) {
          restaurantImageView?.kf.setImage(with: url)
        }
        titleLabel?.text = data_.name
        cityLabel?.text = data_.city
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
