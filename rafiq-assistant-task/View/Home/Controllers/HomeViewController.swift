//
//  HomeViewController.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 2/27/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import UIKit
class HomeViewController: UIViewController{
    
    
    //MARK: - Outlets -
    
    @IBOutlet weak var SpeechLabel: UILabel!
    @IBOutlet weak var RecordButton: UIButton!
    @IBOutlet weak var RecordTypeSwitch: UISwitch!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    //MARK: - Properties -
    
    var presenter : HomePresenterProtocol!
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RecordTypeSwitch.isOn = false
        self.activityIndicator.isHidden = true
        
        presenter = HomePresentere(presenterToHome: self, speechRec:  SpeechRecognition(viewController: self))
        
    }
    
    //MARK: - Actions
    
    @IBAction func RecordButtonClicked(_ sender: Any) {
        
        presenter.recordButtonClicked()
        
    }
    
    
    //MARK: - Helpers -
    
    
   
    
}

extension HomeViewController : PresenterToHome {
    func setSpeechLabelText(text: String) {
        self.SpeechLabel.text = text
    }
    
    func isSearchSwitchOn() -> Bool {
        return self.RecordTypeSwitch.isOn
    }
    
    func setRecordButtonTitle(title: String) {
        self.RecordButton.setTitle(title, for: .normal)
    }
    func startActivityIndicator() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    func endActivityIndicator() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        
    }
    //router
    func presentProductsViewController(restaurants: [RestaurantModel] ){
        
        let VC = self.storyboard?.instantiateViewController(identifier: "RestaurantsViewController") as! RestaurantsViewController
        VC.restaurants = restaurants
        VC.modalPresentationStyle = .overCurrentContext
        self.present(VC, animated: true, completion: nil)
    }
}
