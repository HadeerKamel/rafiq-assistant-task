//
//  HomeBusiness.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/3/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
class HomeBusiness : HomeBusinessProtocol{
    func searchWith(searchWord: String,locationString:String , completion: @escaping ([RestaurantModel]) -> ()) {
        DalHome.getRestaurants(searchWord:searchWord,locationString: locationString){   restaurants in
            completion(restaurants.storesMap.map{RestaurantModel(data: $0)} as [RestaurantModel])
        }
    }
    
    
}
protocol HomeBusinessProtocol {
    func searchWith(searchWord: String ,locationString:String, completion : @escaping([RestaurantModel])->())
}
