//
//  CurrentLocationDetails.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/13/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
import GooglePlaces

class CurrentLocationDetails :  NSObject, CLLocationManagerDelegate{
    private var locationManager = CLLocationManager()
    private var prevLocation = CLLocationCoordinate2D()
    var placeDetails : String = ""
    
    init(completion: @escaping ()->()){
        super.init()
        locationSetup(){ locationString in
            self.placeDetails = locationString ?? ""
            completion()
        }
    }
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if isEmptyLocation(location: prevLocation) {
//            return
//        }
//
//        if let currentLocation = locations.first?.coordinate{
//            if isDiffLocation(location: currentLocation) {
//                print("New Location")
//                locationSetup(){ locationString in
//                    self.placeDetails = locationString ?? ""
//                }
//            }
//        }
       
    }
    private func isEmptyLocation(location: CLLocationCoordinate2D)-> Bool{
        return location.latitude == 0.0 && location.longitude == 0.0
    }
    private func isDiffLocation(location: CLLocationCoordinate2D)-> Bool{
        return !(location.latitude == prevLocation.latitude && location.longitude == prevLocation.longitude)
    }
    private func locationSetup(completion: @escaping(String?)->()){
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        if let currentLocation = locationManager.location{
            getCurrentPlaceID(location: currentLocation) { (currentPlaceID) in
                guard let placeID = currentPlaceID else {
                    completion(nil)
                    return}
                
                DalLocation.getLocationDetails(placeID: placeID) { (locationDetailsString) in
                    guard let placeDetails = locationDetailsString else { completion(nil)
                        return}
                    self.prevLocation = currentLocation.coordinate
                    print(placeDetails)
                    completion(placeDetails)
                }
            }
        }else{
            completion(nil)
        }
    }
    private func getCurrentPlaceID(location: CLLocation , completion: @escaping(String?)->()){
        
        //completion("EjnYp9mE2KrYrdix2YrYsdiMINin2YTYr9mI2KfZiNmK2YbYjCDYudin2KjYr9mK2YbYjCDZhdi12LEiLiosChQKEgkRXnZOzkBYFBE9-xCqVLyqAxIUChIJDclWaMhAWBQR6vxWhNnDpFU")
                   let placesClient = GMSPlacesClient()
                   placesClient.currentPlace { (liklehoodList,error) in
                       if let error = error {
                           print("An error occurred: \(error.localizedDescription)")
                           return
                       }

                       if let nearestPlace = liklehoodList?.likelihoods.first?.place {
                        completion(nearestPlace.placeID)

                       }
                   }
        
    }
}

