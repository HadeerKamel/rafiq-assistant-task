//
//  SpeechRecognition.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 2/27/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import Speech
import AVKit

class SpeechRecognition {
    
    private var viewController : UIViewController
    var isRecording : Bool = false
    private var recognitionRequest : SFSpeechAudioBufferRecognitionRequest?
    private var audioEngine: AVAudioEngine!
    private var inputNode: AVAudioInputNode!
    private var audioSession = AVAudioSession.sharedInstance()

    private var recognizedString = ""
    
    init (viewController: UIViewController){

        self.viewController = viewController
        audioEngine = AVAudioEngine()
        inputNode = audioEngine.inputNode
    }
    func checkPermissions(){
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
        DispatchQueue.main.async {
            switch authStatus {
            case .authorized: break
            default : self.handlePermissionFailed()
            }
            }
            
        }
    }
    private func handlePermissionFailed() {
    
        let ac = UIAlertController(title: ".التطبيق محتاج صلاحيات لإستخدام الميكروفون",
                                   message: ".ياريت تحدث الإعدادات", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "إفتح الإعدادات", style: .default) { _ in
            let url = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(url)
        })
        ac.addAction(UIAlertAction(title: "تم", style: .cancel))
        viewController.present(ac, animated: true)
        
        
       
    }
    private func handleError(withMessage message: String) {
        
        let ac = UIAlertController(title: "جرب تاني", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "تمام", style: .default))
        viewController.present(ac, animated: true)

        
    }
    func startRecording(resultString : @escaping(String)->()) {
        
        guard let recognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ar-EG")), recognizer.isAvailable else {
            handleError(withMessage: "Speech recognizer not available.")
            return
        }
        isRecording = true
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest?.shouldReportPartialResults = false
        
        
        recognizer.recognitionTask(with: recognitionRequest!) { (result, error) in
            guard error == nil else { self.handleError(withMessage: error!.localizedDescription); return }
            guard let result = result else { return }

            print("got a new result: \(result.bestTranscription.formattedString), final : \(result.isFinal)")

            if result.isFinal {
                DispatchQueue.main.async {
                    
                    self.recognizedString = result.bestTranscription.formattedString
                    resultString(self.recognizedString)
                    
                    
                }
            }
        }
        
        
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
            self.recognitionRequest?.append(buffer)
        }
        
        
        audioEngine.prepare()
        
       
        
        do {
            // TODO: Start recognizing speech.
                   audioSession = AVAudioSession.sharedInstance()
                   try audioSession.setCategory(.record, mode: .spokenAudio, options: .duckOthers)
                   try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            try audioEngine.start()
            
            
        } catch {
            handleError(withMessage: error.localizedDescription)
        }
        
        
        
    }
    
    
    func stopRecording()  {
        
    
        // End the recognition request.
        recognitionRequest?.endAudio()
        recognitionRequest = nil
      
        // Stop recording.
        audioEngine.stop()
        inputNode.removeTap(onBus: 0) // Call after audio engine is stopped as it modifies the graph.
      
        // Stop our session.
        try? audioSession.setActive(false)
        audioSession = AVAudioSession.sharedInstance()

        isRecording = false
    }

}
