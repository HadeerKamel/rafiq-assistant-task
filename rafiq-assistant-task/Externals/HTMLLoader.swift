//
//  HTMLLoader.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/2/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class HTMLLoader : UIView ,WKUIDelegate,WKNavigationDelegate {
    
    static let shared = HTMLLoader()
    
    private var webView : WKWebView!
    
    func getHTMLFromURL(urlString: String , completion : @escaping (String)->()){
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        self.addSubview(webView)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        
        let url = URL(string: urlString)
        let urlRequest = URLRequest(url: url!)
        
        webView.load(urlRequest)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+10.0) {[unowned self] in
        self.webView.evaluateJavaScript("document.documentElement.outerHTML",
                                   completionHandler: { (html: Any?, error: Error?) in
                                    print(html)
                                    completion(html as! String)
        })
        }
        
    }
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        DispatchQueue.main.async {
//            webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
//                                       completionHandler: { (html: Any?, error: Error?) in
//                                        print(html)
//            })
//        }
//    }
//    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        print(error)
//    }

}
