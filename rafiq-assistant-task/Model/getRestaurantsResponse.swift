//
//  getRestaurantsResponse.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/13/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation

struct getRestaurantsResponse: Codable{
    let status: String
    let data: StoresMap
    
}

struct StoresMap : Codable{
    var storesMap : [StoreMap] = []
    
    enum CodingKeys: String, CodingKey{
        case storesMap
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let storesMap_ = try values.decodeIfPresent([String:StoreMap].self, forKey: .storesMap)
        
        for item in storesMap_ ?? [:] {
            storesMap.append(item.value)
        }
        
    }
    
}

struct StoreMap: Codable {
    let heroImageUrl : String?
    let citySlug : String?
    let title : String?
}
