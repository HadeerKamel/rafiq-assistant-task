//
//  RestaurantModel.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/3/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
import Kanna

class RestaurantModel : Codable{
    var name : String
    var imageURL: String
    var city: String
    init(data: StoreMap){
        self.name = data.title ?? ""
        self.imageURL = data.heroImageUrl ?? ""
        self.city = data.citySlug ?? ""
    }
    
    
    
    
    
//    static func parseFromHTML(htmlString: String) -> [RestaurantModel]{
//        var restaurants: [RestaurantModel] = []
//
//        if let doc = try? HTML(html: htmlString, encoding: .utf8) {
//
//            print(doc.title)
//
//            for result in doc.css("div[class^='db']") {
//
//                print(result.toHTML)
//
//
//                print(result.css("a, bo ax cm").first?["href"])
//
//                if let restaurantName = result.css("div[class^='ap aq av as dy c1 bi bg']").first?.text , let restaurantLink = result.css("a, bo ax cm").first?["href"]{
//                    restaurants.append(RestaurantModel(name:restaurantName,link:restaurantLink))
//                        print(restaurantName)
//                        print(restaurantLink)
//                }
//
//            }
//
//
//        }
//        return restaurants
//    }
}
