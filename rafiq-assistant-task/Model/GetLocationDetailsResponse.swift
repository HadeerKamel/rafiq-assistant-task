//
//  GetLocationDetailsResponse.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/13/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
struct GetLocationDetailsResponse: Codable{
    let status: String
    let data: String
    
    enum CodingKeys: String, CodingKey{
          case status
          case data
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status) ?? ""
        let locationDetails = try values.decodeIfPresent(LocationData.self, forKey: .data)
       
        let jsonEncoder = JSONEncoder()
        let jsonData = try jsonEncoder.encode(locationDetails)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)
        
        self.data = json ?? ""
    }
}
struct LocationString: Codable {
    let string: String
    
}

struct LocationData : Codable {
    let address : Address?
    let latitude : Double?
    let longitude : Double?
    let reference : String?
    let referenceType : String?
    let type : String?
    let source : String?


}

struct Address : Codable {
    let address1 : String?
    let address2 : String?
    let aptOrSuite : String?
    let city : String?
    let country : String?
    let eaterFormattedAddress : String?
    let postalCode : String?
    let region : String?
    let subtitle : String?
    let title : String?
    let uuid : String?


}
