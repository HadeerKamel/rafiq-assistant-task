//
//  PresenterToHome.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/3/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation

protocol PresenterToHome{
    func setRecordButtonTitle(title: String)
    func setSpeechLabelText(text: String)
    func isSearchSwitchOn() -> Bool
    func startActivityIndicator()
    func endActivityIndicator()
    func presentProductsViewController(restaurants: [RestaurantModel] )
}
