//
//  HomePresenter.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 2/28/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
class HomePresentere: HomePresenterProtocol{
    internal var speechRec: SpeechRecognition?
    private var view : PresenterToHome
    private var business : HomeBusinessProtocol
    private var currentLocation : CurrentLocationDetails!
    
    init (presenterToHome : PresenterToHome ,speechRec: SpeechRecognition){
        self.view = presenterToHome
        self.speechRec = speechRec
        business = HomeBusiness()
        getLocation()
    }
    func getLocation(){
        view.startActivityIndicator()
        currentLocation = CurrentLocationDetails(){
            self.view.endActivityIndicator()
        }
    }
    func recordButtonClicked() {
        setRecordButtonTitle()
        
        if speechRec?.isRecording ?? false {
            speechRec?.stopRecording()
            
        } else {
            speechRec?.startRecording(){  resultString   in
                
                self.view.setSpeechLabelText(text: resultString)
                
                if self.view.isSearchSwitchOn(){
                    
                    self.view.startActivityIndicator()
                    self.business.searchWith(searchWord: resultString , locationString: self.currentLocation.placeDetails) { (restaurants) in
                        self.view.endActivityIndicator()
                        self.view.presentProductsViewController(restaurants: restaurants)
                    }
                }}

            }
    }
    
    func setRecordButtonTitle (){
        (speechRec?.isRecording ?? false) ? view.setRecordButtonTitle(title: "إبتدي تسجيل") :
        view.setRecordButtonTitle(title: "وقف تسجيل")
    }
    
}
protocol HomePresenterProtocol {
    var speechRec : SpeechRecognition? {get set}
    func recordButtonClicked()
}
