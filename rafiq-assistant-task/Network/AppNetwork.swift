//
//  AppNetwork.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 2/29/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
import UIKit
import Kanna
import Alamofire
import WebKit

struct CookieProbertyList {
    let domain: String
    let path: String
    let name: String
    let value: String
    init(domain: String, path: String, name: String,value: String){
        self.domain = domain
        self.path = path
        self.name = name
        self.value = value
    }
}
class AppNetwork : NSObject{
    
    static func callApi(urlString:String ,parameters:[String:Any],method: HTTPMethod,cookie: CookieProbertyList?,completion: @escaping(Data?,Error?)->() ){
        
        guard let url = URL(string: urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? "") else {return}
        let headers : HTTPHeaders  = ["x-csrf-token": "x"]
        
        if let cookieProberties = cookie {
            let cookieProps = [
                HTTPCookiePropertyKey.domain: cookieProberties.domain,
                HTTPCookiePropertyKey.path: cookieProberties.path,
                HTTPCookiePropertyKey.name: cookieProberties.name,
                HTTPCookiePropertyKey.value: cookieProberties.value
            ]
            
            AF.session.configuration.httpShouldSetCookies = true
            if let cookie = HTTPCookie(properties: cookieProps) {
                AF.session.configuration.httpCookieStorage?.setCookie(cookie)
            }
        }
        AF.request(url, method: method, parameters: parameters,encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            
           print(response.debugDescription)
            if response.error == nil {
                completion(response.data, nil)
            }else{
                print(response.error)
                completion(nil, response.error)
            }
          
        }
    }
    static func getHTMLString(urlString: String , completion : @escaping(String)->()){
        
        
        HTMLLoader.shared.getHTMLFromURL(urlString: urlString) { (html) in
            
            completion(  html)
            
        }
        //        AF.request(urlString).responseString { response in
        //            if response.error == nil{
        //                do{
        //                    completion( try response.result.get())
        //                }
        //                catch{
        //                    print("error converting result to string")
        //                }
        //            }else{
        //                print(response.error)
        //            }
        //
        //        }
    }
    
    
}

