//
//  NetworkConstants.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/3/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
class NetworkConstants {
    static let UberEatsLink = "https://www.ubereats.com"
    
    static let getFeed = UberEatsLink + "/api/getFeedV1"
    static let getlocationDetails = UberEatsLink + "/api/getLocationDetailsV1"
    
    static let UberEatsSearchLink = UberEatsLink + "/ar-EG/search?pl=JTdCJTIyYWRkcmVzcyUyMiUzQSUyMiVEOCVBNyVEOSU4NCVEOCVBQSVEOCVBRCVEOCVCMSVEOSU4QSVEOCVCMSUyMiUyQyUyMnJlZmVyZW5jZSUyMiUzQSUyMkVqZEZiQ0JVWVdoeWFYSXNJRUZrSUVSaGQyRjNhVzRzSUVGaVpHVmxiaXdnUTJGcGNtOGdSMjkyWlhKdWIzSmhkR1VzSUVWbmVYQjBJaTRxTEFvVUNoSUpFVjUyVHM1QVdCUVJQZnNRcWxTOHFnTVNGQW9TQ1EzSlZtaklRRmdVRWVyOFZvVFp3NlJWJTIyJTJDJTIycmVmZXJlbmNlVHlwZSUyMiUzQSUyMmdvb2dsZV9wbGFjZXMlMjIlMkMlMjJsYXRpdHVkZSUyMiUzQTMwLjA0NjA1NTQ5OTk5OTk5OCUyQyUyMmxvbmdpdHVkZSUyMiUzQTMxLjIzNTY0MTIlN0Q%3D&q="
}
