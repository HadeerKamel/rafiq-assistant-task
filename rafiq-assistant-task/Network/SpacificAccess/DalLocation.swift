//
//  DalLocation.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/14/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
class DalLocation{
    static func getLocationDetails(placeID : String,completion: @escaping(String?)->()){
        let params = [
        "localeCode" : "ar-EG" ,
        "id" : placeID ,
        "provider":"google_places" ]
        
        AppNetwork.callApi(urlString: NetworkConstants.getlocationDetails, parameters: params, method: .post,cookie: nil ) { (response , error) in
            if error == nil{
                do {
                 let getLocationDetailsREsponse = try JSONDecoder().decode(GetLocationDetailsResponse.self, from: response!)
                    completion(getLocationDetailsREsponse.data)
                }catch{ error
                    print("Can't map locatoin response \(error)")
                    
                }
                
            }
        }
    }
}
