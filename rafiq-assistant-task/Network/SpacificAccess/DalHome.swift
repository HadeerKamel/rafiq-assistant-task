//
//  DalHome.swift
//  rafiq-assistant-task
//
//  Created by Hadeer Kamel on 3/3/20.
//  Copyright © 2020 Hadeer. All rights reserved.
//

import Foundation
enum ResponseStatus : String{
    case success = "success"
    case failure = "failure"
}
class DalHome {
    
    
    static func getRestaurants(searchWord: String,locationString:String,completion: @escaping(StoresMap )->()) {
        
        let params = ["localeCode":"ar-EG" ,
                      "userQuery":searchWord ]//.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? ""]
        
       
        
        let cookie = CookieProbertyList(domain: ".ubereats.com" , path: "/", name:"uev2.loc", value: locationString )
        
        AppNetwork.callApi(urlString: NetworkConstants.getFeed, parameters: params, method: .post,cookie: cookie ) { (response , error) in
            do{
                let getRestaurantsResonse = try JSONDecoder().decode(getRestaurantsResponse.self, from: response!)
                 //print(getRestaurantsResonse)
                completion(getRestaurantsResonse.data)
            }catch{
                
            }
        }
        
        
        
        
//        let urlArabicString =  URLString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? ""
//
//        //   print(urlArabicString)
//
//        AppNetwork.getHTMLString(urlString:  urlArabicString ){ htmlString in
//
//            completion(RestaurantModel.parseFromHTML(htmlString: htmlString))
//
//        }
        
        
    }
    
}
